var OptimizeCssAssetsPlugin = require('optimize-css-assets-webpack-plugin');
var UglifyJsPlugin = require('uglifyjs-webpack-plugin');

module.exports = function (env, argv) {
    return {
        mode: env.production ? 'production' : 'development',
        devtool: env.production ? "source-map" : "cheap-module-eval-source-map",
        optimization: {
            minimizer: [
                new UglifyJsPlugin({
                    uglifyOptions: {
                        mangle: {
                            keep_fnames: true,
                        },
                    },
                })
            ],
        },
        plugins: [
            new OptimizeCssAssetsPlugin(),
        ],
    };
};