//demo file for testing
const add = (a, b) => a + b;
const generateGreeting = (name = `Anonymous`) => `Hello, ${name}!`;

test("should add two nums", () => {
    const result = add(3, 4);
    expect(result).toBe(7);
});

//remove file after checking initial test case