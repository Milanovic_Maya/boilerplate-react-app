console.log("Index.js is running");

import React from "react";
import { render } from "react-dom";
// import { Provider } from "react-redux";
// import configureStore from "./store/configureStore.js";
// import AppRouter from "./routers/AppRouter.js";

// if using react-dates, import css here

// const store=configureStore();
// const app=(<Provider store={store}><AppRouter/></Provider>)

const app = (
    <div>
        <h1>REACT APP BOILERPLATE</h1>
    </div>
);
render(app, document.getElementById("root"));